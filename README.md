# README #

- Funtionality is priority over UI

### How do I get set up? ###

- Fork this git repo to your computer
- Create a branch
- When you are done, push your work and create a pull request to the original repo.

### How you will be tested? ###

- Code clearity
- Code structure
- Unit testing
- Use of design patterns

# Requirements

*Create a 3 page app.*

- When user launchs the app, they should see the addresses on the map. Create a json of any number of address you want or use any API to load addresses)
- Annotions should be clustered on the map
- When user clicks on an annotation user should see a callout with address details and a button to go to the detail of the addess.
- When user clicks on a Detail button it should take user to the details of the address and list of people who have booked that address. (Use mock data as you like)

Example: Just for example purpose.

*Address.json*
```[
   {
      "id":"4sdnfn3mnsdfds2",
      "title":"address1",
      "address1":"100 Rue Guy",
      "address2":"",
      "city":"montreal",
      "state":"Qc",
      "pincode":"J5H3Y7"
   },
   {
      "id":"4sdnfn3232ffsnsdfds2",
      "title":"address2",
      "address1":"121 Rue Guy",
      "address2":"",
      "city":"montreal",
      "state":"Qc",
      "pincode":"J5H4Y7"
   }
]```

*Bookings.json*
```[
   {
      "id":"fsdfsdfdsf",
      "booked_by":"Person Name",
      "address_id":"4sdnfn3mnsdfds2",
      "timings":{
         "mon":"12pm - 2pm",
         "fri":"3pm-5pm"
      }
   },
   {
      "id":"klkjj;kjl",
      "booked_by":"Person Name",
      "address_id":"89723jkhkljhfjsd",
      "timings":{
         "tue":"12pm - 2pm",
         "thr":"3pm-5pm"
      }
   }
]
```

### Who do I talk to? ###

If you have some questinos please don't hesitate to ask anytime @ tarangpatel2@yahoo.co.in or 514-299-5630