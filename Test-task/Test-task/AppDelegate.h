//
//  AppDelegate.h
//  Test-task
//
//  Created by Igor Rykov on 15.04.2018.
//  Copyright © 2018 Igor Rykov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

